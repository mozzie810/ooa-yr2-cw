import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Game 
{
    public static void main(String[] args)
    {
        // Create a Game Factory.
        GameFactory gameFactory = new GameFactory();
        
        // Get user input.
        BufferedReader userAnswer = 
                new BufferedReader(new InputStreamReader(System.in));
        
        String gameType = "";
        
        // Ask the user what type of game they want to play.
        System.out.print("Card (c) or Die (d) game?");

        try
        {
            gameType = userAnswer.readLine();
            IGame newGame = gameFactory.createNewGame(gameType);
            newGame.mainGame();
            newGame.declareWinner();

        }
        catch(IOException ex)
        {
            System.out.println("Please enter either - Card, c, Die, d. ");
        }
        catch (NullPointerException np)
        {
            System.out.println("Please enter something!");
        }
    }
}
