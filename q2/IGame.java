import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;

public interface IGame 
{
    //Variable(s) used in the die game methods.
    public static HashSet<Integer> numbersRolled = new HashSet<Integer>();
            
    // The BufferedReader used throughout.
    public static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    
    // The random number generator used throughout.
    public static ILCG rand = new LCG();

    // Variable(s) used in the card game methods.
    public static HashSet<Card> cardsChosen=new HashSet<Card>();
    
    // Main game loop.
    public void mainGame();
    
    // Win or Lose.
    public void declareWinner();
}
