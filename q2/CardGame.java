
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class CardGame implements IGame
{
    public static Deck deckOfCards = new Deck();

    public CardGame() 
    {
        // Shuffle the Deck of cards.
        deckOfCards.shuffle();
        
        // Print out the result, get 52 cards for this game.
        System.out.println(deckOfCards.getCards(52));
    }
    
    public void mainGame() 
    {
        // The main game loop.
        // Let user select two cards from the pack as this game requires that.
        for (int i=0; i<2; i++) 
        {
            System.out.println("Hit <RETURN> to choose a card");
            try
            {
                br.readLine();
            }
            catch(IOException ex)
            {
                System.out.println("Warning: Input Exception!");
            }

            int cardChoice=(int) (rand.getNextNumber() * deckOfCards.cards.size());
            System.out.println("You chose " + deckOfCards.cards.get(cardChoice));
            cardsChosen.add(deckOfCards.cards.remove(cardChoice));
        }

        // Display the cards chosen and remaining cards
        System.out.println("Cards chosen: " + cardsChosen);
        System.out.println("Remaining cards: " + deckOfCards.cards.toString());

    }

    public void declareWinner() 
    {
        // Declare the winner:
        System.out.println("Cards chosen: " + cardsChosen);

        // User wins if one of them is an Ace 
        if (cardsChosen.toString().trim().contains("ace"))
        {
            System.out.println("You won!");
        }
        else 
        {
            System.out.println("You lost!");
        }
    }
}
