public class GameFactory
{
    public IGame createNewGame(String gameType) 
    {
        if (gameType.equalsIgnoreCase("c") || 
                gameType.equalsIgnoreCase("card")) 
        {
          return new CardGame();
        }
        else if (gameType.equalsIgnoreCase("d") || 
                gameType.equalsIgnoreCase("die")) 
        {
          return new DiceGame();
        }
        else 
        {
            return null;
        }
    }
}
