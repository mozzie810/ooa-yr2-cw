import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck 
{
    List<Card> cards;

    public Deck() 
    {
        // Create Array List of cards.
        cards = new ArrayList<>();
        
        // For the different suits available in suits, add a new card.
        Card.Rank[] ranks = Card.Rank.values();
        Card.Suit[] suits = Card.Suit.values();
        for (Card.Rank rank : ranks) 
        {
            for (Card.Suit suit : suits) 
            {
                // Add a card to the deck for every suit.
                cards.add(new Card(rank, suit)); 
            }
        }

    }

    public void shuffle() 
    {
        // Shuffle the cards.
        Collections.shuffle(cards);
    }

    public List<Card> getCards(int amount) 
    {
        // Get a certain amount of cards as specified.
        List<Card> out = new ArrayList();
        for(int i = 0; i < amount; i++) 
        {
            out.add(cards.get(i));
        }
        return out;
    }
}