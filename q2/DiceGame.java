
import java.io.IOException;

public class DiceGame implements IGame
{
    public void mainGame()
    {
        // Main Game Loop.
        for (int i=0; i<2; i++) 
        {
          System.out.println("Hit <RETURN> to roll the die");
          try
          {
              // Attempt reading line
            br.readLine();
          }
          catch(IOException ex)
          {
              System.out.println("Warning: Input Exception!");
          }
          int dieRoll=(int)(rand.getNextNumber() * 6) + 1;

          // Tell user what they rolled.
          System.out.println("You rolled " + dieRoll);
          numbersRolled.add(dieRoll);
        }

        // Display the numbers rolled.
        System.out.println("Numbers rolled: " + numbersRolled);
    }

    public void declareWinner()
    {
          // Declare the winner:

          // User wins if at least one of the die rolls is a 1
          if (numbersRolled.contains(new Integer(1))) 
          {
            System.out.println("You won!");
          }
          else 
          {
              System.out.println("You lost!");
          }
    }
}
