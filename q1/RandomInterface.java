public interface RandomInterface 
{
    // Simply defines a method for retrieving the next random number
    public double getNextNumber();
    public double next();
}
