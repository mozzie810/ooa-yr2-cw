# OOA yr2 cw

This was done and submitted in my second year of uni, but I am putting here for reference.

### Question 1
(This is worth 30% of the total marks available for the present exercise.)
Download the ZIP archive containing the files Game.java, LinearCongruentialGenerator.java,
RandomInterface.java and IncompatibleRandomInterface.java from Learning Central. A
listing of these files is included in the present coursework specification for your convenience.
This program will not work, as Game.java is expecting a LinearCongruentialGenerator
defined differently from the way it is defined in the download files.
* “Fix” this problem by altering LinearCongruentialGenerator to implement RandomInterface.
* Draw a UML class diagram representing the program which results from the above
modifications.
(Note: if you use yUML then you will not be able to underline static (class) methods or
attributes. In that case, indicate in an explanatory note which ones you would have
underlined if you could!)
iii) Write a description of the purpose of this program, bearing in mind that you are going to
use this description to help you with identifying suitable classes for an improved version
of this program in Question 2. [Guide: between 150 and 300 words]

### Question 2
(This is worth 70% of the total marks available for the present exercise.)
The program considered in Question 1 above is deliberately inelegant. The purpose of Question 2
is to design and implement an improved version which:
* Shows evidence of your having applied principles relating to concepts such as coupling
and cohesion

* Makes use of the description created in answer to Question 1 to help your identification of
suitable classes

* Separates out the two game implementations. (You should apply some kind of Factory
pattern, or a related pattern, in order to achieve this.1
)
* Includes brief comments explaining the code
    * Identify suitable classes for an improved version of the program, taking into account the
points listed above, and explain your choice. [Guide: you may well choose to present
these results in tabular form, with just a very small number of words explaining the role of
each class, plus a small number of additional sentences explaining your overall choice]
    * Draw a UML class diagram representing the improved program you are going to
implement.
    * Implement the improved program!
